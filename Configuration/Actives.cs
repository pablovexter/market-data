using System;

namespace teste.Configuration
{
    public class Actives
    {
        public string GetWinAssetName()
        {
            DateTime today = DateTime.Now;
            if (today.Month % 2 != 0)
            {
                today = today.AddMonths(1);
            }

            if (IsCurrentWinExpired())
            {
                today = today.AddMonths(2);
            }

            string currentYear = today.Year.ToString().Substring(2, 2);

            return $"WIN{GetLetterFromMonth(today.Month)}{currentYear.ToString()}";
        }


        public string GetWdoAssetName()
        {
            DateTime today = DateTime.Now;

            if (DateTime.DaysInMonth(today.Year, today.Month) == today.Day)
            {
                today = today.AddMonths(2);
            }
            else
            {
                today = today.AddMonths(1);
            }
            string currentYear = today.Year.ToString().Substring(2, 2);

            return $"WDO{GetLetterFromMonth(today.Month)}{currentYear}";
        }

        public bool IsCurrentWinExpired()
        {
            DateTime today = DateTime.Now;
            if (today.Month % 2 != 0)
            {
                return false;
            }

            DateTime fifteenthDay = new DateTime(today.Year, today.Month, 15);
            DateTime dueDate;

            int expirationDay = 15;
            int wednesday = 3;
            int dayOfWeek = (int)fifteenthDay.DayOfWeek;

            if (dayOfWeek > wednesday)
            {
                expirationDay -= dayOfWeek - wednesday;
                dueDate = new DateTime(fifteenthDay.Year, today.Month, expirationDay);
            }
            else if (dayOfWeek < wednesday)
            {
                expirationDay += wednesday - dayOfWeek;
                dueDate = new DateTime(fifteenthDay.Year, today.Month, expirationDay);
            }
            else
            {
                dueDate = new DateTime(fifteenthDay.Year, today.Month, expirationDay);
            }

            dueDate = dueDate.AddDays(-1);

            return today > dueDate;
        }

        public char GetLetterFromMonth(int month)
        {
            //No c# a contagem dos meses começa em 1, na array a contagem dos elementos começa em 0
            //O primeiro caracter da array NÃO equivale a nenhuma regra de negócio, é utilizado apenas como sentinela para que o item de index 1 seja o mesmo do mês 1
            char[] letters = new char[] { '#', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'Q', 'U', 'V', 'X', 'Z' };

            return letters[month];
        }
    }
}