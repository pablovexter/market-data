using System;
using Newtonsoft.Json;
using System.Globalization;
using Microsoft.Extensions.Options;
using teste.Domains.Models;

namespace teste.Configuration
{
    public class RouteQuotes
    {
        private readonly MarketDataGenialConfig _marketDataGenialConfig;
        private double lastWinValue;
        private double lastWdoValue;

        private DateTime WdoTime = DateTime.Now;
        private DateTime WinTime = DateTime.Now;
        private Object WdoLock = new Object();
        private Object WinLock = new Object();

        public RouteQuotes(
            IOptions<MarketDataGenialConfig> marketDataGenialConfig
        )
        {
            this._marketDataGenialConfig = marketDataGenialConfig.Value;
        }

        public void Route(string message)
        {
            QuoteModel quote = JsonConvert.DeserializeObject<QuoteModel>(message.ToString());

            string symbol = quote.marketDataIncrementalRefresh.mdEntries[0].Symbol;
            string quoteValue = quote.marketDataIncrementalRefresh.mdEntries[0].MDEntryPx.mantissa;

            DateTime dateNow = DateTime.Now;
        }

        private Int32 CalculateMilliseconds(DateTime startDate, DateTime endDate)
        {
            return Convert.ToInt32(endDate.Subtract(startDate).TotalMilliseconds);
        }

        private double FormatQuote(string symbol, string quoteValue)
        {
            if (symbol.ToUpperInvariant().Contains("WDO") && quoteValue.Length >= 5)
            {
                quoteValue = $"{quoteValue.Substring(0, quoteValue.Length - 1)}.{quoteValue.Substring(quoteValue.Length - 1, 1)}";
            }

            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            provider.NumberGroupSeparator = ",";

            return Convert.ToDouble(quoteValue, provider);
        }
    }
}