namespace teste.Configuration
{
    public class MarketDataGenialConfig
    {
        public string Username { get; set; }
        public string SocketSession { get; set; }
        public string Port { get; set; }
        public string IP { get; set; }
        public int Delay { get; set; }
    }
}