using System;
using System.IO;

namespace teste.Configuration
{
    public class LogFile
    {
        private readonly string _ordersLogFolderPath;
        private readonly string _assetsLogFolderPath;
        private readonly string _errorsLogFolderPath;
        private readonly string _quotesLogFolderPath;
        private readonly string _snapshotLogFolderPath;
        private readonly string _othersLogFolderPath;
        private readonly DateTime currentDate = DateTime.Now;

        public LogFile()
        {
            _ordersLogFolderPath = @"./logs/orders";
            _errorsLogFolderPath = @"./logs/errors";
            _assetsLogFolderPath = @"./logs/assets";
            _quotesLogFolderPath = @"./logs/quotes";
            _snapshotLogFolderPath = @"./logs/snapshot";
            _othersLogFolderPath = @"./logs/snapshot";
        }

        public void SaveOrders(string message)
        {
            try
            {
                if (!Directory.Exists(_ordersLogFolderPath))
                {
                    Directory.CreateDirectory(_ordersLogFolderPath);
                }

                message = $"{DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff %K]")} - {message}";

                try
                {
                    File.AppendAllText(this.getLogFileName(_ordersLogFolderPath), message + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exception)
                {
                    this.Errors(exception.ToString());
                }
            }
            catch (Exception exception)
            {
                this.Errors(exception.ToString());
            }
        }

        public void SaveAssets(string message)
        {
            try
            {
                if (!Directory.Exists(_assetsLogFolderPath))
                {
                    Directory.CreateDirectory(_assetsLogFolderPath);
                }

                message = $"{DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff %K]")} - {message}";

                try
                {
                    File.AppendAllText(this.getLogFileName(_assetsLogFolderPath), message + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exception)
                {
                    this.Errors(exception.ToString());
                }
            }
            catch (Exception exception)
            {
                this.Errors(exception.ToString());
            }
        }

        public void SaveQuotes(string message)
        {
            try
            {
                if (!Directory.Exists(_quotesLogFolderPath))
                {
                    Directory.CreateDirectory(_quotesLogFolderPath);
                }

                message = $"{DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff %K]")} - {message}";

                try
                {
                    File.AppendAllText(this.getLogFileName(_quotesLogFolderPath), message + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exception)
                {
                    this.Errors(exception.ToString());
                }
            }
            catch (Exception exception)
            {
                this.Errors(exception.ToString());
            }
        }

        public void SaveSnapshots(string message)
        {
            try
            {
                if (!Directory.Exists(_snapshotLogFolderPath))
                {
                    Directory.CreateDirectory(_snapshotLogFolderPath);
                }

                message = $"{DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff %K]")} - {message}";

                try
                {
                    File.AppendAllText(this.getLogFileName(_snapshotLogFolderPath), message + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exception)
                {
                    this.Errors(exception.ToString());
                }
            }
            catch (Exception exception)
            {
                this.Errors(exception.ToString());
            }
        }

        public void SaveOthers(string message)
        {
            try
            {
                if (!Directory.Exists(_othersLogFolderPath))
                {
                    Directory.CreateDirectory(_othersLogFolderPath);
                }

                message = $"{DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff %K]")} - {message}";

                try
                {
                    File.AppendAllText(this.getLogFileName(_othersLogFolderPath), message + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exception)
                {
                    this.Errors(exception.ToString());
                }
            }
            catch (Exception exception)
            {
                this.Errors(exception.ToString());
            }
        }

        public void Errors(string error)
        {
            Console.WriteLine(error);

            try
            {
                if (!Directory.Exists(_errorsLogFolderPath))
                {
                    Directory.CreateDirectory(_errorsLogFolderPath);
                }

                try
                {
                    File.AppendAllText(this.getLogFileName(_errorsLogFolderPath), this.getTopLine());
                    File.AppendAllText(this.getLogFileName(_errorsLogFolderPath), error + Environment.NewLine);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private string getLogFileName(string folderPath)
        {
            return Path.Combine(folderPath, $"{currentDate.ToString("yyyy-MM-dd")}.log");
        }

        private string getTopLine()
        {
            return $"{DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff %K]")}{Environment.NewLine}";
        }
    }
}