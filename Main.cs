using System;
using System.Threading;
using System.Threading.Tasks;
using teste.Domains.Interfaces;
using teste.Gateways.TcpSocket;

namespace teste
{
    public class Main
    {
        private readonly SocketConnection _socketConnection;
        private readonly SocketMessage _socketMessage;
        private readonly IMarketDataMessageFactory _marketDataMessageFactory;

        public Main(
            SocketConnection socketConnection,
            SocketMessage socketMessage,
            IMarketDataMessageFactory marketDataMessageFactory)
        {
            _socketConnection = socketConnection;
            _socketMessage = socketMessage;
            _marketDataMessageFactory = marketDataMessageFactory;
        }

        public void Run()
        {
            Task.Run(() => {
                _socketConnection.Start();

                if (_socketConnection.IsConnected())
                {
                    Console.WriteLine($"## Connected successfully! ##");

                    _socketMessage.Send(_marketDataMessageFactory.CreateLogonMessage());

                    _socketMessage.Read(_socketConnection.GetSocket());
                }

            }).ConfigureAwait(false);

            this.SendHeartBeat();
        }

        private void SendHeartBeat()
        {
            Timer heartBeat = new Timer(_ => _socketMessage.Send(_marketDataMessageFactory.CreateHeartbeatMessage()), null, 0, 10000);

            Console.ReadLine();
        }
    }
}