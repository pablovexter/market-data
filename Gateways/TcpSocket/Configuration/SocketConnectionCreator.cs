using System;
using System.Net;
using System.Net.Sockets;
using teste.Configuration;
using Microsoft.Extensions.Options;

namespace teste.Gateways.TcpSocket.Configuration
{
    public class SocketConnectionCreator
    {
        private readonly MarketDataGenialConfig _marketDataGenialConfig;

        public SocketConnectionCreator(IOptions<MarketDataGenialConfig> marketDataGenialConfig)
        {
            _marketDataGenialConfig = marketDataGenialConfig.Value;
        }

        public Socket Create()
        {
            return new Socket(IPAddress.Parse(_marketDataGenialConfig.IP).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect(ref Socket socket, IPEndPoint ipEndPoint)
        {
            try
            {
                socket.Connect(ipEndPoint);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}