using System;
using System.Net;
using Microsoft.Extensions.Options;
using teste.Configuration;

namespace teste.Gateways.TcpSocket.Configuration
{
    public class IpEndPointCreator
    {
        private readonly MarketDataGenialConfig _marketDataGenialConfig;
        
        public IpEndPointCreator(IOptions<MarketDataGenialConfig> marketDataGenialConfig)
        {
            _marketDataGenialConfig = marketDataGenialConfig.Value;
        }

        public IPEndPoint Create()
        {
            return new IPEndPoint(IPAddress.Parse(_marketDataGenialConfig.IP), Convert.ToInt32(_marketDataGenialConfig.Port));
        }
    }
}