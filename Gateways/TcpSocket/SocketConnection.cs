using System;
using System.Net;
using System.Net.Sockets;
using Microsoft.Extensions.Options;
using teste.Configuration;
using teste.Gateways.TcpSocket.Configuration;

namespace teste
{
    public class SocketConnection
    {
        private readonly MarketDataGenialConfig _marketDataGenialConfig;
        private readonly SocketConnectionCreator _socketConnectionCreator;
        private readonly IpEndPointCreator _ipEndPointCreator;
        private IPEndPoint _ipEndPoint;

        private static Socket socket;

        public SocketConnection(
            IOptions<MarketDataGenialConfig> marketDataGenialConfig,
            SocketConnectionCreator socketConnectionCreator,
            IpEndPointCreator ipEndPointCreator)
        {
            _marketDataGenialConfig = marketDataGenialConfig.Value;
            _socketConnectionCreator = socketConnectionCreator;
            _ipEndPointCreator = ipEndPointCreator;
        }

        public void Start()
        {
            if (!(SocketConnection.socket is Socket))
            {
                SocketConnection.socket = _socketConnectionCreator.Create();
                _ipEndPoint = _ipEndPointCreator.Create();

                Console.WriteLine($"## Connecting to {_ipEndPoint}... ##");

                _socketConnectionCreator.Connect(ref SocketConnection.socket, _ipEndPoint);

                if (!this.IsConnected())
                {
                    throw new SocketException();
                }
            }
        }

        public Socket GetSocket()
        {
            return SocketConnection.socket;
        }

        public bool IsConnected()
        {
            if (SocketConnection.socket is Socket)
            {
                return !((SocketConnection.socket.Poll(1, SelectMode.SelectRead)
                    && (SocketConnection.socket.Available == 0))
                    || !SocketConnection.socket.Connected);
            }
            else
            {
                return false;
            }
        }
    }
}