using System;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Collections.Generic;
using teste.Configuration;
using teste.Domains.Models;
using teste.Domains.Protobuf;
using teste.Domains.Interfaces;

namespace teste.Gateways.TcpSocket
{
    public class SocketMessage
    {
        private readonly SocketConnection _socketConnection;
        private readonly IMarketDataMessageFactory _marketDataMessageFactory;
        private readonly Actives _actives;
        private readonly RouteQuotes _routeQuotes;
        private readonly LogFile _logFile;

        private List<AssetModel> _assetsList;
        byte[] bytesToReceive = new byte[20480];
        List<byte> holdoverByts = new List<byte>();
        int nextReadType = 0;
        int nextReadSize = 4;

        public SocketMessage(
            SocketConnection socketConnection,
            IMarketDataMessageFactory marketDataMessageFactory,
            Actives actives,
            RouteQuotes routeQuotes,
            LogFile logFile
        )
        {
            _socketConnection = socketConnection;
            _marketDataMessageFactory = marketDataMessageFactory;
            _actives = actives;
            _routeQuotes = routeQuotes;
            _logFile = logFile;

            _assetsList = new List<AssetModel>();
        }

        public void Send(byte[] message)
        {
            try
            {
                if (_socketConnection.IsConnected())
                {
                    byte[] convertedMessage = BitConverter.GetBytes(message.Length);

                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(convertedMessage);
                    }

                    _socketConnection.GetSocket().Send(convertedMessage, 0, 4, System.Net.Sockets.SocketFlags.None);
                    _socketConnection.GetSocket().Send(message, 0, message.Length, System.Net.Sockets.SocketFlags.None);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        public void Read(Socket socket)
        {
            while (_socketConnection.IsConnected())
            {
                try
                {
                    int size = socket.Receive(bytesToReceive);

                    holdoverByts.AddRange(bytesToReceive.Take(size));

                    while (holdoverByts.Count >= nextReadSize)
                    {
                        if (nextReadType == 0)
                        {
                            byte[] tmp = holdoverByts.Take(4).ToArray();

                            if (BitConverter.IsLittleEndian)
                            {
                                Array.Reverse(tmp);
                            }

                            nextReadSize = BitConverter.ToInt32(tmp, 0);

                            nextReadType = 1;

                            holdoverByts.RemoveRange(0, 4);
                        }
                        else
                        {
                            if (nextReadSize <= holdoverByts.Count)
                            {
                                SiliconFeederProtocol incomingMessage = null;
                                incomingMessage = SiliconFeederProtocol.Parser.ParseFrom(holdoverByts.Take(nextReadSize).ToArray());

                                this.CheckMessage(incomingMessage);

                                holdoverByts.RemoveRange(0, nextReadSize);

                                nextReadSize = 4;
                                nextReadType = 0;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            Console.WriteLine($"## Connection lost! ##");

            Environment.Exit(0);
        }

        private void CheckMessage(SiliconFeederProtocol incomingMessage)
        {
            if ((incomingMessage.Session is Session)
                && incomingMessage.Session.MessageTypeCase.ToString() == "Heartbeat")
            {
                byte[] heartBeatMessage = _marketDataMessageFactory.CreateHeartbeatMessage();

                this.Send(heartBeatMessage);
            }
            else if ((incomingMessage.Session is Session)
               && incomingMessage.Session.MessageTypeCase.ToString() == "LogonReturn")
            {
                byte[] securityListRequestMessage = _marketDataMessageFactory.CreateSecurityListRequestMessage();

                this.Send(securityListRequestMessage);
            }
            else if (incomingMessage.MessageTypeCase.ToString() == "SecurityList")
            {
                String wdoAssetName = _actives.GetWdoAssetName();
                String WinAssetName = _actives.GetWinAssetName();

                _logFile.SaveOrders(Convert.ToString(incomingMessage.SecurityList.RelatedSymSecGroup));

                foreach (SiliconFeederProtocol.Types.SecurityList.Types.RelatedSymSecListGroup Active in incomingMessage.SecurityList.RelatedSymSecGroup)
                {
                    AssetModel asset = JsonConvert.DeserializeObject<AssetModel>(Active.ToString());

                    // if (!(asset.underlyings is underlyings[]))
                    // {
                    //     Console.WriteLine(asset.Symbol);
                    // }

                    // if (asset.underlyings is underlyings[] && asset.underlyings[0].UnderlyingSecurityExchange != "XBMF")
                    // {
                    //     Console.WriteLine(asset.Symbol);
                    // }

                    _logFile.SaveAssets(asset.Symbol.ToUpperInvariant());

                    if (_assetsList.Count < 21 && asset.Symbol.Length == 5)
                    {
                        _assetsList.Add(asset);
                    }

                    // if (asset.Symbol.ToUpperInvariant().Equals("RANI4") || asset.Symbol.ToUpperInvariant().Equals("OIBR3"))
                    // {
                    //     Console.WriteLine($"## Asset found: {asset.Symbol.ToUpperInvariant()} ##");

                    //     _assetsList.Add(asset);
                    // }
                }

                if (incomingMessage.SecurityList.LastFragment)
                {
                    Console.WriteLine($"## Last Fragment of Security List received. ##");

                    this.SendAssets(this._assetsList);
                }
            }
            else if (incomingMessage.MessageTypeCase.ToString() == "MarketDataIncrementalRefresh")
            {
                Task.Factory.StartNew(() =>
                {
                    _logFile.SaveQuotes(Convert.ToString(incomingMessage));
                });
            }
            else if (incomingMessage.MessageTypeCase.ToString() == "MarketDataSnapshotFullRefresh")
            {
                _logFile.SaveSnapshots(Convert.ToString(incomingMessage));
            }
            else
            {
                _logFile.SaveOthers(Convert.ToString(incomingMessage));
            }

        }

        private void SendAssets(List<AssetModel> assetsList)
        {
            foreach (AssetModel asset in assetsList)
            {
                byte[] marketDataMessage = _marketDataMessageFactory.CreateMarketDataRequestMessage(asset);

                this.Send(marketDataMessage);
            }
        }
    }
}