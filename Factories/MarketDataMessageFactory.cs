using System;
using Google.Protobuf;
using teste.Configuration;
using teste.Domains.Models;
using teste.Domains.Protobuf;
using teste.Domains.Interfaces;
using Microsoft.Extensions.Options;

namespace teste.Factories
{

    public class MarketDataMessageFactory : IMarketDataMessageFactory
    {
        private readonly MarketDataGenialConfig _marketDataGenialConfig;

        public MarketDataMessageFactory(IOptions<MarketDataGenialConfig> marketDataGenialConfig)
        {
            _marketDataGenialConfig = marketDataGenialConfig.Value;
        }

        public byte[] CreateHeartbeatMessage()
        {
            SiliconFeederProtocol outgoingMessage = this.CreateAndInitializeProtocolMessage();

            outgoingMessage.Session = new Session();
            outgoingMessage.Session.Heartbeat = new Session.Types.Heartbeat();
            outgoingMessage.Session.Heartbeat.Payload = "heartbeat";

            return outgoingMessage.ToByteArray();
        }

        public byte[] CreateLogonMessage()
        {
            SiliconFeederProtocol outgoingMessage = this.CreateAndInitializeProtocolMessage();

            outgoingMessage.Session = new Session();
            outgoingMessage.Session.Logon = new Session.Types.Logon();
            outgoingMessage.Session.Logon.SessionName = _marketDataGenialConfig.SocketSession;
            outgoingMessage.Session.Logon.Username = _marketDataGenialConfig.Username;

            Console.WriteLine($"## Sending Logon message ##");

            return outgoingMessage.ToByteArray();
        }

        public byte[] CreateSecurityListRequestMessage()
        {
            SiliconFeederProtocol outgoingMessage = this.CreateAndInitializeProtocolMessage();

            outgoingMessage.SecurityListRequest = new SiliconFeederProtocol.Types.SecurityListRequest();
            outgoingMessage.SecurityListRequest.SubscriptionRequestType = "0";

            Console.WriteLine($"## Sending Security List Request message ##");

            return outgoingMessage.ToByteArray();
        }

        public byte[] CreateMarketDataRequestMessage(AssetModel asset)
        {
            SiliconFeederProtocol outgoingMessage = this.CreateAndInitializeProtocolMessage();

            outgoingMessage.MarketDataRequest = new SiliconFeederProtocol.Types.MarketDataRequest();
            outgoingMessage.MarketDataRequest.MDReqID = new Guid().ToString();


            var symbol = new SiliconFeederProtocol.Types.MarketDataRequest.Types.RelatedSymMDRequestItem();
            symbol.Symbol = asset.Symbol;
            symbol.SecurityID = asset.SecurityID;
            symbol.SecurityIDSource = "8";

            outgoingMessage.MarketDataRequest = new SiliconFeederProtocol.Types.MarketDataRequest();
            outgoingMessage.MarketDataRequest.SubscriptionRequestType = "1";

            outgoingMessage.MarketDataRequest.MDEntryType.Add("X");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("Z");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("0");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("1");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("e");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("f");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("2");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("4");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("5");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("7");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("8");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("9");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("B");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("a");
            outgoingMessage.MarketDataRequest.MDEntryType.Add("c");

            outgoingMessage.MarketDataRequest.RelatedSymMDRequests.Add(symbol);

            Console.WriteLine("\nMARKET DATA REQUEST");
            Console.WriteLine(outgoingMessage);
            Console.WriteLine("MARKET DATA REQUEST\n");



            Console.WriteLine($"## Sending Market Data Request message for Asset {asset.Symbol} - {asset.SecurityID} ##");

            return outgoingMessage.ToByteArray();
        }

        private SiliconFeederProtocol CreateAndInitializeProtocolMessage()
        {
            SiliconFeederProtocol protocolMessage = new SiliconFeederProtocol();
            string dateTime = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            protocolMessage.SendingTime = ulong.Parse(dateTime);

            return protocolMessage;
        }
    }
}