﻿using System.IO;
using teste.Factories;
using teste.Configuration;
using teste.Gateways.TcpSocket;
using teste.Domains.Interfaces;
using Microsoft.Extensions.Configuration;
using teste.Gateways.TcpSocket.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace teste
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            serviceProvider.GetService<Main>().Run();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", false)
            .Build();

            serviceCollection.AddOptions();

            serviceCollection.Configure<MarketDataGenialConfig>(configuration.GetSection("MarketDataGenialConfig"));

            serviceCollection.AddTransient<Main>();
            serviceCollection.AddSingleton<SocketConnection>();
            serviceCollection.AddSingleton<SocketConnectionCreator>();
            serviceCollection.AddSingleton<IpEndPointCreator>();
            serviceCollection.AddTransient<SocketMessage>();
            serviceCollection.AddTransient<Actives>();
            serviceCollection.AddSingleton<RouteQuotes>();
            serviceCollection.AddTransient<IMarketDataMessageFactory, MarketDataMessageFactory>();
            serviceCollection.AddTransient<LogFile>();
        }
    }
}
