using teste.Domains.Models;

namespace teste.Domains.Interfaces
{
    public interface IMarketDataMessageFactory
    {
        byte[] CreateLogonMessage();
        byte[] CreateMarketDataRequestMessage(AssetModel asset);
        byte[] CreateHeartbeatMessage();
        byte[] CreateSecurityListRequestMessage();
    }
}