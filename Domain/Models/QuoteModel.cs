namespace teste.Domains.Models
{
    public class QuoteModel
    {
        public MdEntries marketDataIncrementalRefresh { get; set; }
    }

    public class MdEntries
    {
        public Entries[] mdEntries { get; set; }
    }

    public class Entries
    {
        public string Symbol { get; set; }

        public EntryPx MDEntryPx { get; set; }
    }

    public class EntryPx
    {
        public string mantissa { get; set; }
    }
}