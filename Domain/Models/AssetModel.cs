namespace teste.Domains.Models
{
    public class AssetModel
    {
        public string Symbol { get; set; }
        public string SecurityID { get; set; }
        public underlyings[] underlyings { get; set; }
    }

    public class underlyings
    {
        public string UnderlyingSecurityExchange { get; set; }
    }
}